terraform {
  backend "s3" {
    bucket = "aj25-terraform-s3-backend"
    key    = "harness_tfstate/tfstate"
    region = "us-east-1"
  }
}